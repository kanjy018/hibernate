package com.johnk.Hibernate_first_project;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.hibernate.cfg.Configuration;

public class CRUDCommands {
	
	
	public List<Customer> findAllStudentsWithJpql() {
    	// Create configuration
    	Configuration configuration = new Configuration();
    	configuration.configure("hibernate.cfg.xml");
    	configuration.addAnnotatedClass(Customer.class);
    	
    	SessionFactory sessionFactory = configuration.buildSessionFactory();
    	
    	//initialise the session object
    	Session session = sessionFactory.openSession();
    	
		Transaction tx = null;
		tx = session.beginTransaction();
		List<Customer> theList = session.createQuery("select c from Customer c", Customer.class).list();
		tx.commit();
		return theList;
	}
	
	public void createUser(String firstName, String lastName) {
    	// Create configuration
    	Configuration configuration = new Configuration();
    	configuration.configure("hibernate.cfg.xml");
    	configuration.addAnnotatedClass(Customer.class);
    	
    	SessionFactory sessionFactory = configuration.buildSessionFactory();
    	
    	//initialise the session object
    	Session session = sessionFactory.openSession();
    	
    	Customer firstCustomer = new Customer(firstName, lastName);
    	
    	session.beginTransaction();
    	session.save(firstCustomer);
    	
    	session.getTransaction().commit();
	}
	
	public void deleteUser(int id) {
    	// Create configuration
    	Configuration configuration = new Configuration();
    	configuration.configure("hibernate.cfg.xml");
    	configuration.addAnnotatedClass(Customer.class);
    	
    	SessionFactory sessionFactory = configuration.buildSessionFactory();
    	
    	//initialise the session object
    	Session session = sessionFactory.openSession();
    	
    	Customer cust = session.get(Customer.class, id);
    	
    	session.beginTransaction();
    	session.delete(cust);
    	
    	session.getTransaction().commit();
	}
	
	public void updateUser(int id, String firstName, String lastName) {
    	// Create configuration
    	Configuration configuration = new Configuration();
    	configuration.configure("hibernate.cfg.xml");
    	configuration.addAnnotatedClass(Customer.class);
    	
    	SessionFactory sessionFactory = configuration.buildSessionFactory();
    	
    	//initialise the session object
    	Session session = sessionFactory.openSession();
    	
    	Customer cust = session.get(Customer.class, id);
    	
    	if(firstName == null) {
    		cust.setLastName(lastName);
    	} else if(lastName == null) {
    		cust.setFirstName(firstName);
    	} else {
    		System.out.println("No values given");
    	}
    	
    	session.beginTransaction();
    	session.update(cust);
    	
    	session.getTransaction().commit();
	}

}
