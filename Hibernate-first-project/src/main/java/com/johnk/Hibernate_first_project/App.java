package com.johnk.Hibernate_first_project;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.internal.build.AllowSysOut;


public class App 
{
    public static void main( String[] args )
    {
    	// Create configuration
    	Configuration configuration = new Configuration();
    	configuration.configure("hibernate.cfg.xml");
    	configuration.addAnnotatedClass(Customer.class);
    	
    	//Create session factory
    	SessionFactory sessionFactory = configuration.buildSessionFactory();
    	
    	//initialise the session object
    	Session session = sessionFactory.openSession();
    	
    	
    	CRUDCommands crud = new CRUDCommands();
    	
    	//crud.createUser("Thomas", "JamesMcDongle");
    	
    	//crud.deleteUser(1);
    	
    	crud.updateUser(2, null, "Samanata");
    	
    	List<Customer> listOfUsers = crud.findAllStudentsWithJpql();
    	
    	listOfUsers.forEach(System.out::println);

    	
    	//session.getTransaction().commit();
    }
}
