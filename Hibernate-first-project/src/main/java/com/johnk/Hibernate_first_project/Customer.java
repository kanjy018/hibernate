package com.johnk.Hibernate_first_project;


import java.io.Serializable;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import javax.persistence.Column;


@Entity
@Table(name = "customers")
public class Customer {

	@Id
	// Auto Increment must be on in the Db
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int custID;
	
	private String firstName;
	
	private String lastName;
	
	public Customer() {
		
	}
	
	public Customer(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "Customer [custID=" + custID + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	

}
